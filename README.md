## **Corona Stats - India**

Corona Stats - India is a simple and minimalistic tool to keep you updated about the current national and state-wise stats of the Corona virus in India. You can access the tool by visiting the live link available below. Furthermore, the tool can be installed to your device as a PWA for quicker access.

**Gitlab -** https://gitlab.com/veddandekar6/coronaStats-India

**Live Link -** https://veddandekar6.gitlab.io/coronaStats-India/

## **Features**

- Quick and simple one page UI

- Supports offline usage by installing the app on device(PWA)

- State-wise count of confirmed, deaths and recovered cases

- Flat and simple graphs are data distribution among the states

- 5 day data comparison chart for the country


**Note:** In order to install the PWA, visit the live link above and select the "add to home screen" option that pops up. Alternatively, you can do the same by tapping on the three-dot menu on your browser and selecting "Add to home screen".

## **API**

Special thanks to https://www.covid19india.org/ for providing their API.

**Star if you like what you see. Feel free to point out issues and submit PRs.**
